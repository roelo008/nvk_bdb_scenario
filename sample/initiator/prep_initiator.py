"""
Brief script to prepare Land Use statistics from Scenariokaart to be used in INITIATOR model
Hans Roelofsen, WEnR, april 2021
"""

import os
import rasterio as rio
import geopandas as gp
import pandas as pd
import numpy as np
import rasterstats as rast


def pxls2ha(row, res=2.5):
    """
    function to apply
    :param row: list of pxl counts
    :param res: pxl resolution in m
    :return: area in ha
    """
    return np.divide(np.multiply(np.sum(row), np.square(res)), 10000)


# Landbouw Deelgebieden Shapefile
lbdg = gp.read_file(r'w:\PROJECTS\Nvk_bdb\a_geodata\c_achtergrond\LandbouwDeelgebied\LandbouwDeelgebieden.shp')

# BDB Scenariokaart
bdb_kaart = rio.open(r'w:\PROJECTS\Nvk_bdb\a_geodata\b_2050\c_basiskaart\bdb2050_t10xlsx-20210421-095811_firstrule.tif')
bdb_arr = bdb_kaart.read(1)
bdb_tab = gp.read_file(r'w:\PROJECTS\Nvk_bdb\a_geodata\b_2050\c_basiskaart\bdb2050_t10xlsx-20210421-095811_firstrule.tif.vat.dbf')

bdb_descs = {'dierlijke_productie': ["rule317", "rule318", "rule319", "rule320", "rule369", "rule370", "rule371", "rule372", "rule373", "rule388", "rule389", "rule390", "rule391", "rule392", "rule393", "rule394", "rule395", "rule396", "rule397", "rule398", "rule399", "rule400", "rule401", "rule402", "rule403", "rule404", "rule405", "rule406", "rule407", "rule408", "rule409", "rule410", "rule411", "rule412", "rule413", "rule414", "rule415", "rule416", "rule417", "rule418"],
             'aardappels': ["rule290", "rule293", "rule295", "rule306", "rule308", "rule339", "rule341", "rule356", "rule358", "rule436", "rule449", "rule463", "rule484"],
             'granen': ["rule299", "rule301", "rule312", "rule314", "rule345", "rule347", "rule362", "rule364", "rule441", "rule461", "rule464"],
             'suikerbieten': ["rule297", "rule310", "rule343", "rule360", "rule458"],
             'overige_gewassen': ["rule294", "rule296", "rule300", "rule307", "rule309", "rule313", "rule340", "rule342", "rule346", "rule357", "rule359", "rule363", "rule435", "rule436", "rule437", "rule438", "rule439", "rule440", "rule446", "rule448", "rule453", "rule454", "rule455", "rule456", "rule457", "rule460", "rule462", "rule482", "rule483"]}

bdb_df = pd.DataFrame(rast.zonal_stats(vectors=lbdg, raster=bdb_arr, affine=bdb_kaart.transform,
                                       nodata=bdb_kaart.nodata, categorical=True, stats='count',
                                       category_map=dict(zip(bdb_tab.Value, bdb_tab.Descriptio.str[:7]))))
# Add each category as column to LandbouwDeelgebieden Shapefile
for k, v in bdb_descs.items():
    lbdg['BDB_{}_ha'.format(k)] = bdb_df.loc[:, list(set(v).intersection(set(list(bdb_df))))].apply(pxls2ha, axis=1)
del bdb_df, bdb_arr, bdb_kaart

# HUIDIG kaart
huidig_kaart = rio.open(r'w:\PROJECTS\Nvk_bdb\a_geodata\a_huidig\c_basiskaart\nvk_huidig-20201126-100144_newval.tiff')
huidig_arr = huidig_kaart.read(1)
huidig_tab = gp.read_file(r'w:\PROJECTS\Nvk_bdb\a_geodata\a_huidig\c_basiskaart\nvk_huidig-20201126-100144_newval.tiff.vat.dbf')
huidig_descs = {'dierlijke_productie': ['Grasland, evt met riet',
                                        'botanisch grasland en randen basis',
                                        'botanisch grasland en randen plus',
                                        "nietBA_plaagongevoellig gras (beweiding) weidevogelbeheer",
                                        "nietBA_plaagongevoellig gras (beweiding)",
                                        "nietBA_plaagongevoellig gras (maaien) weidevogelbeheer",
                                        "nietBA_plaagongevoellig gras (maaien)",
                                        "Weidevogelbeheer"],
                'aardappels': ["nietBA_plaaggevoellig consumptieaardappelen",
                               "nietBA_plaaggevoellig pootaardappelen",
                               "nietBA_plaaggevoellig zetmeelaardappelen"],
                'granen': ["welBA_plaaggevoellig zomergerst",
                           "nietBA_plaaggevoellig wintertarwe",
                           "nietBA_plaaggevoellig zomergerst"],
                'suikerbieten': ["nietBA_plaaggevoellig suikerbieten"],
                'overige_gewassen': ["welBA_plaaggevoellig overige gewassen (luzerne_soja)",
                                     "welBA_plaaggevoellig overige gewassen (aardbei_koolzaad)",
                                     "welBA_plaaggevoellig overige gewassen (raapzaad_komkommer)",
                                     "welBA_plaaggevoellig overige gewassen (courgette_pompoen)",
                                     "welBA_plaaggevoellig sperziebonen",
                                     "welBA_plaaggevoellig sperziebonen (veldbonen)",
                                     "nietBA_plaagongevoellig overige gewassen",
                                     "nietBA_plaaggevoellig bloemkool",
                                     "nietBA_plaaggevoellig overige gewassen",
                                     "nietBA_plaaggevoellig prei",
                                     "nietBA_plaaggevoellig sla",
                                     "nietBA_plaaggevoellig snijmais",
                                     "nietBA_plaaggevoellig spruitkool",
                                     "nietBA_plaaggevoellig winterpeen",
                                     "nietBA_plaaggevoellig zaaiuien"]}

huidig_df = pd.DataFrame(rast.zonal_stats(vectors=lbdg, raster=huidig_arr, affine=huidig_kaart.transform,
                                          nodata=huidig_kaart.nodata, categorical=True, stats='count',
                                          category_map=dict(zip(huidig_tab.Value, huidig_tab.Descriptio))))
for k, v in huidig_descs.items():
    lbdg['HUIDIG_{}_ha'.format(k)] = huidig_df.loc[:, list(set(v).intersection(set(list(huidig_df))))] \
                                              .apply(pxls2ha, axis=1)

del huidig_tab, huidig_arr, huidig_kaart
lbdg.drop('geometry', axis=1).to_csv(r'w:\PROJECTS\Nvk_bdb\a_geodata\a_huidig\d_afgeleiden\b_tabellen\a_initiator\LandBouwDeelGebied_Huidig_BDB.csv',
            index=False, sep=',')

