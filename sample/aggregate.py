"""
Aggregation tool for geospatial rasters.


usage: aggregate.py [-h] [--prio PRIO] [--th TH] [--ignore IGNORE]
                    src dest cf {SUM,MAXIMUM,MINIMUM,MEDIAN,MEAN}

positional arguments:
  src                   input raster.
  dest                  name output raster w/o extension.
  cf                    cell factor.
  {SUM,MAXIMUM,MINIMUM,MEDIAN,MEAN}
                        aggregation method.

optional arguments:
  -h, --help            show this help message and exit
  --prio PRIO           value to prioritise.
  --th TH               Threshold for priority value
  --ignore IGNORE       Value to ignore

See below for details.

Hans Roelofsen, WEnR, october 2020.

"""

import argparse
import os
import arcpy
import gdal

arcpy.CheckOutExtension("spatial")


def aggregate(in_rast, cf, method, ignore, prio, th):
    """
    Aggregate raster to a larger file size. Wrapper around
    https://desktop.arcgis.com/en/arcmap/10.3/tools/spatial-analyst-toolbox/aggregate.htm
    :param in_rast: input raster on disk
    :param cf: "The factor by which to multiply the cell size of the input raster to obtain the desired resolution for the output raster."
    :param method: Aggregation type. Options: ['SUM', 'MAXIMUM', 'MINIMUM', 'MEDIAN', 'MEAN']
    :param ignore: Optional value to ignore in the aggregation
    :param prio: Optional value to prioritise in the aggregation if it occurs >= th
    :param th: Treshold for priority value
    :return: arcpy raster object
    """

    # Read as raster
    print('Reading raster {0}'.format(in_rast))
    rast = arcpy.Raster(in_rast)

    if ignore is not None:
        print('setting {} to null'.format(ignore))
        rast = arcpy.sa.SetNull(in_conditional_raster=rast,
                                in_false_raster_or_constant=rast,
                                where_clause='Value = {}'.format(ignore))
        print(arcpy.GetMessages())

    # Aggregate on data-cells only
    print('\nAggregate')
    rast_agg = arcpy.sa.Aggregate(in_raster=rast, cell_factor=cf, aggregation_type=method)
    print(arcpy.GetMessages())

    if prio is not None:
        prio_rast = arcpy.sa.Con(in_conditional_raster=rast,
                                 in_true_raster_or_constant=1,
                                 in_false_raster_or_constant=0,
                                 where_clause='Value = {}'.format(prio))
        print(arcpy.GetMessages())

        prio_rast_agg = arcpy.sa.Aggregate(prio_rast, cell_factor=cf, aggregation_type='sum')
        print(arcpy.GetMessages())

        prio_rast_sel = arcpy.sa.Con(in_conditional_raster=prio_rast_agg,
                                     in_true_raster_or_constant=prio,
                                     in_false_raster_or_constant=rast_agg,
                                     where_clause='Value >= {}'.format(th))
        print(arcpy.GetMessages())
        rast_agg = prio_rast_sel

    if ignore is not None:
        # Reinstate ingnored values
        print('\nSetting nulls again')
        rast_nulls = arcpy.sa.IsNull(rast_agg)
        rast_out = arcpy.sa.Con(in_conditional_raster=rast_nulls,
                                in_true_raster_or_constant=ignore,
                                in_false_raster_or_constant=rast_agg,
                                where_clause="Value = 1")
        print(arcpy.GetMessages())
        return rast_out
    else:
        return rast_agg


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('src', help='input raster.')
    parser.add_argument('dest', help='name output raster w/o extension.')
    parser.add_argument('cf', help='cell factor.', type=int)
    parser.add_argument('method', help='aggregation method.', choices=['SUM', 'MAXIMUM', 'MINIMUM', 'MEDIAN', 'MEAN'])
    parser.add_argument('--prio', help='value to prioritise.', type=int, default=None)
    parser.add_argument('--th', help='Threshold for priority value', type=int, default=None)
    parser.add_argument('--ignore', help='Value to ignore', type=int, default=None)
    parser.add_argument('--of', help='Output format', choices=['geotiff', 'map', 'flt'], default='geotiff')
    args = parser.parse_args()

    if args.prio and args.th is None:
        raise argparse.ArgumentTypeError('provide treshold value.')

    out_dir = os.path.dirname(args.dest)
    if not os.path.isdir(out_dir):
        raise argparse.ArgumentTypeError('Not a directory: {}.'.format(os.path.dirname(args.dest)))
    scratch_dir = os.path.join(out_dir, 'scratch')
    if not os.path.isdir(scratch_dir):
        os.makedirs(scratch_dir)
    arcpy.env.scratchWorkspace = scratch_dir

    if not arcpy.Exists(args.src):
        raise argparse.ArgumentTypeError('Not a file: {}.'.format(args.src))

    try:
        out = aggregate(in_rast=args.src, cf=args.cf, method=args.method, prio=args.prio, th=args.th, ignore=args.ignore)
        out.save(args.dest)
    except Exception as e:
        print(e)
        raise
