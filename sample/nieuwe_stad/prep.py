import rasterio as rio
import numpy as np

rast = rio.open(r'w:\PROJECTS\Nvk_bdb\a_geodata\b_2050\a_brondata\4_WLOStad\b_resampled\nieuwe_stad_2050.tif')
arr = rast.read(1)
arr_out = np.where(arr > 254, 0, arr)

prof = rast.profile
with rio.open(r'c:\apps\z_temp\nieuwe_stad_2050_v3.tif', 'w', **prof) as dest:
    dest.write(arr_out, indexes=1)