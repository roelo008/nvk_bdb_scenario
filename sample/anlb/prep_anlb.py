"""
This script takes the ANLb Shapefile and prepares it for merging into the scenario map.

Input: shapefile export from ~\\wur\dfs-root\PROJECTS\EcoSysDiensten_WEnR\b_data\a_GedeeldeData\BiologischeLandbouw_RobSmidt 2019\ANB_BioLB_2018.gdb\ANLB123_union_NDB_TK

Processing
step 1: Reclass 'Beheercode' into one of four categories:
 1	agrarische natuur, aka weidevogelbeheer
 2	agrarische natuur bosjes Deciduous forest, aka agrarische bosjes en heggen
 3	agrarische natuur meerjarig, bloemarm (semi) Natural grassland aka botanisch grasland en randen basis
 4	agrarische natuur meerjarig, bloemrijk (semi) Natural grassland	aka botanisch grasland en randen plus
 Reclass is n:1 and some beheercodes may not be categorized into a new category. See 1 LU kaart voor alle ecosysteemdiensten_v6.xlsx
 for reclass rules

step 2: rasterize to 10m with BNL extent

by Hans Roelofsen, august 2020
"""

import os
import geopandas as gp
import gdal
import datetime

# source data
data_dir = r'c:/apps/temp_geodata/bdb/anlb/'
anlb_src = r'shp/ANB123_union_ANLB_NDB_TK.shp'
rast_out_dir = r'raster/'
shp_out_dir = r'shp_tussenstap'
out_basename = 'anlb120_U_NDB_TK_hdr'
timestamp =  datetime.datetime.now().strftime("%y%m%d-%H%M")

# reclass dicts
beheercode2bdbclass = {
'10a': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'10b': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'11a': 'agrarische natuur',
'11b': 'agrarische natuur',
'12a': 'agrarische natuur',
'12b': 'agrarische natuur',
'12c': 'agrarische natuur',
'13a': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'13b': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'13c': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'13d': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'13e': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'13f': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'17a': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'17b': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'18a': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'18b': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'18c': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'18e': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'19a': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'19b': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'19c': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'19d': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'19e': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'19f': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'19g': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'19h': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'1a': 'agrarische natuur',
'1b': 'agrarische natuur',
'1c': 'agrarische natuur',
'1d': 'agrarische natuur',
'1e': 'agrarische natuur',
'1f': 'agrarische natuur',
'1g': 'agrarische natuur',
'1i': 'agrarische natuur',
'1k': 'agrarische natuur',
'1l': 'agrarische natuur',
'1m': 'agrarische natuur',
'1n': 'agrarische natuur',
'1o': 'agrarische natuur',
'1q': 'agrarische natuur',
'1r': 'agrarische natuur',
'1s': 'agrarische natuur',
'1t': 'agrarische natuur',
'20a': 'agrarische natuur bosjes Deciduous forest',
'20b': 'agrarische natuur bosjes Deciduous forest',
'20c': 'agrarische natuur bosjes Deciduous forest',
'20d': 'agrarische natuur bosjes Deciduous forest',
'20e': 'agrarische natuur bosjes Deciduous forest',
'21a': 'agrarische natuur bosjes Deciduous forest',
'22a': 'agrarische natuur bosjes Deciduous forest',
'22b': 'agrarische natuur bosjes Deciduous forest',
'23a': 'agrarische natuur bosjes Deciduous forest',
'23b': 'agrarische natuur bosjes Deciduous forest',
'24a': 'agrarische natuur bosjes Deciduous forest',
'25a': 'agrarische natuur bosjes Deciduous forest',
'26a': 'agrarische natuur bosjes Deciduous forest',
'26b': 'agrarische natuur bosjes Deciduous forest',
'27a': 'agrarische natuur bosjes Deciduous forest',
'27b': 'agrarische natuur bosjes Deciduous forest',
'28a': 'agrarische natuur bosjes Deciduous forest',
'29a': 'agrarische natuur bosjes Deciduous forest',
'2a': 'agrarische natuur',
'2b': 'agrarische natuur',
'2c': 'agrarische natuur',
'2d': 'agrarische natuur',
'2e': 'agrarische natuur',
'30a': 'agrarische natuur',
'31a': 'agrarische natuur meerjarig, bloemarm (semi) Natural grassland',
'31b': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'32a': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'3a': 'agrarische natuur',
'3b': 'agrarische natuur',
'3c': 'agrarische natuur',
'3d': 'agrarische natuur',
'3e': 'agrarische natuur',
'3f': 'agrarische natuur',
'3g': 'agrarische natuur',
'3i': 'agrarische natuur',
'3j': 'agrarische natuur',
'3k': 'agrarische natuur',
'5a': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'5b': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'5c': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'5d': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'5e': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'5f': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'5g': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'5h': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'5i': 'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland',
'6a': 'agrarische natuur',
'6b': 'agrarische natuur',
'6c': 'agrarische natuur',
'8a': 'agrarische natuur',
'8b': 'agrarische natuur',
'8c': 'agrarische natuur',
'8d': 'agrarische natuur',
'8e': 'agrarische natuur',
'8f': 'agrarische natuur',
'9a': 'agrarische natuur',
'9b': 'agrarische natuur',
'9c': 'agrarische natuur',
'9d': 'agrarische natuur',
'9e': 'agrarische natuur',
'9f': 'agrarische natuur',
'9g': 'agrarische natuur',
'9h': 'agrarische natuur',
'9i': 'agrarische natuur',
'1h': 'agrarische natuur',
'3l': 'agrarische natuur'
}
bdbclass2pxl = {
  'agrarische natuur': 1,
  'agrarische natuur bosjes Deciduous forest': 2,
  'agrarische natuur meerjarig, bloemarm (semi) Natural grassland': 3,
  'agrarische natuur meerjarig, bloemrijk (semi) Natural grassland': 4
}

# read source shapefile
anlb = gp.read_file(anlb_src)

# drop features in which we are not interested
anlb.drop(anlb[~anlb.CODE_BEHEE.isin(list(beheercode2bdbclass.keys()))].index, inplace=True)

# two new columns with BDB category and number
anlb['bdb_cat'] = anlb.CODE_BEHEE.map(beheercode2bdbclass)
anlb['bdb_val'] = anlb.bdb_cat.map(bdbclass2pxl)

# write to shapefile
anlb.loc[:, ['bdb_cat', 'bdb_val', 'geometry']].\
    to_file(os.path.join(data_dir, shp_out_dir, '{0}_interm.shp'.format(out_basename)))

# rasterize
"""
Use OSGeoShell 

# 10m
gdal_rasterize -at -a bdb_val -l anlb120_U_NDB_TK_hdr_interm -of GTiff -te 0 300000 280000 625000 -tr 10 10 -ts 28000 32500 -ot Byte -a_srs epsg:28992 -co COMPRESS=LZW anlb120_U_NDB_TK_hdr_interm.shp ..\raster\anlb123_U_NDB_TK_hdr20200812.tiff

# 2.5m
gdal_rasterize -at -init 0 -a bdb_val -l anlb120_U_NDB_TK_hdr_interm -of GTiff -te 0 300000 280000 625000 -tr 2.5 2.5 -ts 112000 130000 -ot UInt8 -a_srs epsg:28992 -co COMPRESS=LZW anlb120_U_NDB_TK_hdr_interm.shp ..\raster\2-5m\anlb123_U_NDB_TK_hdr20200824.tiff


onderstaande lijkt niet te werken. Wellicht is RasterizeLayer beter. 
my_options = gdal.RasterizeOptions(
    format='GTiff',
    outputType=gdal.gdalconst.GDT_Byte,
    outputBounds=[0, 300000, 280000, 625000],
    width=28000,
    height=32500,
    xRes=10,
    yRes=10,
    allTouched=True,
    attribute='bdb_val')
gdal.Rasterize(destNameOrDestDS=os.path.join(data_dir, rast_out_dir, '{0}2_{1}.tif'.format(out_basename, timestamp)),
               srcDS=os.path.join(data_dir, shp_out_dir, '{0}_interm.shp'.format(out_basename)), options=my_options)
"""