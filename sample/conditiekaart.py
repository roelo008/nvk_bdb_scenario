"""
Tool to manipulate an existing abiotic-condition geospatial raster using a scenario-generated raster carrying
instructions to manipulate existing values.

Designed to generate abiotic-condition geospatial rasters for ESD models and MNP in which scenario Land Use changes
are reflected.

Parameters:
    1. original:      original abiotic condition raster
    2. template_rast: raster where pixel values guide applicaiton of the modification rules
    3. template_tab:  *.dbf mapping template pixel values to the template category descriptions
    4. mod_xls:       xlsx linking template descriptions to the modification rules for abiotic condition <abiotic>.
    5. abiotic:       str, column name in <mod_xls>, linking template categories to the final modificaiton rules
    5. template_name: str, column name in <mod_xls>
Output:
    1. geospatial raster identical to `original` but with raster values modified.

Steps:
   1. make dataframe template_pxl_value(int)|template_desc(str)|mod_rule_str(str)|mod_rule(function)
   2. make operations dictionary template_pxl_value <-> mod_rule
   3. read (portion of) original
   4. read (portion of) template_rast
   5. create new rast as np.array([operations[i](x) for i, x in zip(template_rast, original)])

By: Hans Roelofsen, WEnR, april 2021
"""

import os
import argparse
import rasterio as rio
import pandas as pd
import numpy as np
import geopandas as gp
import datetime
import sys

sys.path.append(r'c:\apps\proj_code\benb_utils')
import benb as bb

parser = argparse.ArgumentParser()
parser.add_argument('original', help='original raster', type=bb.geospatial_raster)
parser.add_argument('template', help='template raster', type=bb.geospatial_raster)
parser.add_argument('mod_xls', help='xls name with mod rules')
parser.add_argument('sheet', help='sheet name within the xls')
parser.add_argument('abiotic', help='name of the abiotic condition', type=str)
parser.add_argument('unit', help='measurement unit of the abiotic condition', type=str)
parser.add_argument('template_name', help='name of the template', type=str)
parser.add_argument('--out_dir', help='output directory', type=bb.readable_dir, default='./')
args = parser.parse_args()

# Verify input (not all, this can be expanded later)
bb.file_of_type(args.mod_xls, "xlsx")
assert args.abiotic != args.template_name, "hey, opletten!"

# Timestamps
ts_full = timestamp_full = datetime.datetime.now().strftime('%d-%b-%Y_%H:%M:%S')
xls_created_ts = datetime.datetime.fromtimestamp(os.path.getctime(args.mod_xls)).strftime('%d-%b-%Y_%H:%M:%S')
xls_mod_ts = datetime.datetime.fromtimestamp(os.path.getmtime(args.mod_xls)).strftime('%d-%b-%Y_%H:%M:%S')

print('\n\nStarting {}\n'.format(ts_full))

# Read original and template and check compatibility
original = rio.open(args.original)
template = rio.open(args.template)
assert original.bounds == template.bounds, "unequal bounds"
assert original.res == template.res, "unequal resolution"

# Mapping between template category descriptions and operations. Add key 'geen' for situations where no reclass rule is
# applied, assuming original raster should be retained there.
try:
    rules_df = pd.read_excel(args.mod_xls, sheet_name=args.sheet)
    source_rasters = [x for x in list(rules_df) if x.lower() not in ['newval', 'description', 'remark', 'reclass',
                                                                     'newvalue', 'new_val'] and not x.startswith('_')]

    col_rename_dict = dict(zip([x for x in rules_df.columns if x not in source_rasters and not x.startswith('_')],
                               [x.lower() for x in rules_df.columns if x not in source_rasters and not x.startswith('_')]))
    rules_df.rename(columns=col_rename_dict, inplace=True)
except PermissionError as e:
    print('Please close {} to proceed.'.format(os.path.basename(args.mod_xls)))
    sys.exit(1)

if args.template_name == '_firstrule':
    rules_df['_firstrule'] = ['rule{:03}'.format(i) for i in rules_df.index]
    rules_df['_firstrule'] = rules_df._firstrule.str.cat(rules_df['remark'], sep='_')

try:
    assert args.abiotic in list(rules_df), "{0} is not part of the reclass XLS, try one of {1}".format(args.abiotic,
                                                                                                      '\n'.join(list(rules_df)))
    assert args.template_name in list(rules_df), "{} is not part of the reclass XLS".format(args.template_name)
except AssertionError as e:
    print(e)
    sys.exit(1)

template_desc2operation = dict(zip(rules_df.loc[:, args.template_name], rules_df.loc[:, args.abiotic]))
template_desc2operation['geen'] = 'huidig'

# Each description must be coupled to only one operation, though similar operations may be coupled to different
# descriptions. Verify.
pairs = set([p for p in zip(rules_df.loc[:, args.template_name], rules_df.loc[:, args.abiotic])])
descs = pd.Series([i for i, _ in pairs]).value_counts()
if any(descs > 1):
    duplicates = descs[descs.index[descs > 1]]
    sel_pairs = [(i, j) for i, j in pairs if i in duplicates]
    print(sel_pairs)
    print('no 1:1 pairing between {0} and {1}, please inspect.'.format(args.template_name, args.abiotitc))
    sys.exit(1)
else:
    print('Verified description to operation coupling.')

# Mapping between template pixel values and operations
template_tab = gp.read_file('{}.vat.dbf'.format(args.template))
template_tab['operation_p1'] = template_tab.Descriptio.map(template_desc2operation)
template_tab.operation_p1.fillna('huidig', inplace=True)  #
template_tab['operation_p2'] = ['lambda huidig: {}'.format(x) for x in template_tab.operation_p1.to_list()]
template_tab['operation'] = template_tab.operation_p2.apply(eval)
template_pxl2operations = dict(zip(template_tab.Value, template_tab.operation))
template_pxl2operations[template.nodata] = lambda huidig: huidig

# Verify all lambdas and get min, max values
for row in template_tab.itertuples():
    try:
        out = row.operation(10)
    except NameError as e:
        print('lamda for template description {0} does not work: {1}'.format(row.Descriptio, e))
        sys.exit(1)

# DType must be float
out_dtype = np.float32

# Create output profile
prof = original.profile
prof.update(pixeltype='float', dtype=out_dtype, driver='GTiff', crs=rio.crs.CRS.from_epsg(28992))

# Output name and extension
out_name = '{0}_{1}'.format(os.path.splitext(os.path.basename(args.mod_xls))[0], args.abiotic)
if len(out_name) >= 24:
    out_name = out_name[:24]
destination = os.path.join(args.out_dir, '{}.tif'.format(out_name))

# Write to new file.
with rio.open(destination, 'w', **prof) as dest:

    # Metadata tags (only works for geotiffs)
    dest.update_tags(who_am_i=destination,
                     creation_date=timestamp_full,
                     created_by='{0}@{1}'.format(os.getenv('USERNAME'), os.getenv('COMPUTERNAME')),
                     src_file=args.original,
                     content='{0}[{1}]'.format(args.abiotic, args.unit),
                     remap_xls='{0} sheet {1}'.format(args.mod_xls, args.sheet),
                     remap_xls_created=xls_created_ts,
                     remap_xls_modified=xls_mod_ts,
                     spatial_template_image=args.template)

    # Write per line
    for line in bb.progress_bar(iterable=range(0, original.height), suffix='Complete', length=50,
                                prefix="{:<25}".format('Reclass to {0}'.format(args.abiotic))):

        # Read/Write window of 1 row, full width
        rw_window = rio.windows.Window(col_off=0, row_off=line, width=original.width, height=1)

        # Read portions of template and original rasters
        template_arr = template.read(1, window=rw_window)[0, :]  # shape (11200, )
        original_arr = original.read(1, window=rw_window)[0, :]  # shape (11200, )

        # flattened array with the operations as specified by the template pixel values
        operation_arr = np.array([template_pxl2operations[i] for i in template_arr.flatten()])

        # output values are returns of operation array with original values as parameters
        out_arr = np.array([op(param) for op, param in zip(operation_arr, original_arr)]).astype(out_dtype)

        # Write to file
        dest.write(out_arr.reshape(1, original.width), indexes=1, window=rw_window)



