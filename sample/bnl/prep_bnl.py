"""
Prepare rasters from BNL2019 for inclusion into the BDB scenario- and basiskaart. Objective is to aggregate from 2.5 to
10m resolution. Zeros need to be included in the 10m version because subsequent 'Combine' does not handle NoData well,
but Zeros need to be omitted from the Aggregation operation.
Hans Roelofsen, 18082020
"""

import arcpy
import os


arcpy.CheckOutExtension("spatial")


def agg(in_dir, in_rast, cell_factor, agg_type, avoid_zero=True):

    # Read as raster
    print('Reading raster {0}'.format(in_rast))
    rast = arcpy.Raster(os.path.join(in_dir, in_rast))

    # Set all ZEROS to NoData
    if avoid_zero:
        print('\nSetting null')
        rast_nulled = arcpy.sa.SetNull(in_conditional_raster=rast,
                                       in_false_raster_or_constant=rast,
                                       where_clause='Value = 0')
        print(arcpy.GetMessages())
    else:
        rast_nulled = rast

    # Aggregate on data-cells only
    print('\nAggregate')
    rast_agg = arcpy.sa.Aggregate(in_raster=rast_nulled,
                                  cell_factor=cell_factor,
                                  aggregation_type=agg_type)
    print(arcpy.GetMessages())

    # Reinstate zeros on NoData
    if avoid_zero:
        print('\nSetting nulls again')
        rast_nulls = arcpy.sa.IsNull(rast_agg)
        rast_out = arcpy.sa.Con(in_conditional_raster=rast_nulls,
                                in_true_raster_or_constant=0,
                                in_false_raster_or_constant=rast_agg,
                                where_clause="Value = 1")
        print(arcpy.GetMessages())
    else:
        rast_out = rast_agg
    return rast_out






newval = r'c:\apps\temp_geodata\bdb\z_out\20201019\nvk_huidig_newval_202010191522.tiff'
aggr = aggregate(in_rast=newval, cf=4, method='median', prio=88, th=3)
aggr.save(r'c:\apps\temp_geodata\bdb\z_out\20201019\nvk_huidig_newval_202010191522_10m.tif')

bomen = r'C:/apps/temp_geodata/test/Top10Bomen_crop2.tif'
boom_agg = aggregate(bomen, cf=4, method='median', ignore=0)
boom_agg.save('C:/apps/temp_geodata/test/Top10Bomen_crop2_AGProTest.tif')

t10_dir = r'W:\PROJECTS\EcoSysDiensten_WEnR\b_data\a_GedeeldeData\BasisbestandNatuurLandschap\tiff'
bedrijven_in = 'Bedrijventerrein_Pos5_201.tif'
boom_in = 'Top10Bomen_20191029.tif'
sloot_in = 'Top10Sloot_TOP10NL_2018_s.tif'
vlak_in = 'Top10Vlak_2018_20191008.tif'
oab_in = 'OAD_bbk_T102018_20191112.tif'

bt_dir = r'W:\PROJECTS\EcoSysDiensten_WEnR\b_data\a_GedeeldeData\neergeschaalde_btypen_20190730111601\orig_resolutie'
bt_tiff = r'nbt_20190730111601_U32.tif'

oab = agg(t10_dir, oab_in, 4, 'MEDIAN', avoid_zero=False)
oab.save(r'\\wur\dfs-root\PROJECTS\EcoSysDiensten_WEnR\b_data\c_ScenarioSpecifiek\BDB_Basiskaart\bronbestanden\oad\10m\oab_T102018_10m.tif')

'''
bedrijven = agg(t10_dir, bedrijven_in, 4, 'MEDIAN')
bedrijven.save(r'c:\apps\temp_geodata\bdb\bedrijventerreinen\bnl2019_bedrijf_10m.tif')


bomen = agg(t10_dir, boom_in, 4, 'MEDIAN')
bomen.save(r'c:\apps\temp_geodata\bdb\top10\t10boom.tif')

sloten = agg(t10_dir, sloot_in, 4, 'MEDIAN')
sloten.save(r'c:\apps\temp_geodata\bdb\top10\t10_sloot.tif')

vlak = agg(t10_dir, vlak_in, 4, 'MEDIAN')
vlak.save(r'c:\apps\temp_geodata\bdb\top10\t10_vlak.tif')

bt = agg(bt_dir, bt_tiff, 4, 'MEDIAN')
bt.save(r'c:\apps\temp_geodata\bdb\nbt\10m_resolutie\nbt_20190730111601_U32_10m.tif')
'''