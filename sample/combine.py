"""
Command line tool for combining raster datasets into a single raster, using arcpy.combine.
See: https://pro.arcgis.com/en/pro-app/tool-reference/spatial-analyst/combine.htm
By: Hans Roelofsen, WEnR, 18-08-2020
"""

import arcpy
from arcpy import env
import argparse
import os
import datetime

parser = argparse.ArgumentParser()
parser.add_argument('output', help='relative path and filename of output basename')
parser.add_argument('ins', help='relative paths to all input files', nargs='+')
parser.add_argument('--test', help='test input and ouput params', action='store_true')
parser.add_argument('--vat', help='export vat to csv', action='store_true')
args = parser.parse_args()

env.workspace = os.getcwd()
arcpy.CheckOutExtension('spatial')

inputs = args.ins
testing = args.test

outdir = os.path.dirname(args.output)
basename, ext = os.path.splitext(os.path.basename(args.output))
outname = '{0}_{1}{2}'.format(basename, datetime.datetime.now().strftime('%Y%m%d'), ext)
output = os.path.join(os.getcwd(), outdir, outname)

if testing:
    print('Found {0} rasters to combine:'.format(len(inputs)))
    for i, in_rast in enumerate(inputs, start=1):
        if arcpy.Exists(in_rast):
            print('{0}: {1}'.format(i, in_rast))
        else:
            raise Exception('{} does not exist'.format(in_rast))

    assert os.path.isdir(os.path.join(os.getcwd(), outdir)), '{0} is not a valid output directory'.format(outdir)
    print('\nOutput will be: {0}'.format(output))

else:
    print('Starting @ {0}'.format(datetime.datetime.now().strftime('%d %b %Y %H:%M:%S')))
    outCombine = arcpy.sa.Combine(inputs)
    print(arcpy.GetMessages())
    outCombine.save(output)


'''
python c:\apps\proj_code\MultiReclassTool\a_scripts\combine.py z_combined\2-5m\bdb_250cm_combi.tif \anlb\raster\2-5m\anlb123_U_NDB_TK_hdr20200824.tiff \bedrijventerreinen\2-5m\Bedrijventerrein_Pos5_201.tif \brp\raster\2-5m\brp2018_hdr20200824.tiff \nbt\2-5_resolutie\nbt_20190730111601_U32.tif \top10\2-5m\Top10Bomen_20191029.tif \top10\2-5m\Top10Sloot_TOP10NL_2018_s.tif \top10\2-5m\Top10Vlak_2018_20191008.tif --test

'''
