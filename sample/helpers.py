import os
import argparse
import rasterio as rio


def geospatial_raster(x):
    """
    Is x a geospatial raster?
    :param x: file path
    :return: x if True else argparse.ArgumentTypeError
    """
    if os.path.isfile(x):
        try:
            rast = rio.open(x)
            return x
        except(rio.errors.RasterioIOError, rio.errors.FileError, rio._err.CPLE_OpenFailed) as e:
            raise argparse.ArgumentTypeError(e)
    else:
        raise argparse.ArgumentTypeError('Not a file: {0}'.format(x))


def progress_bar(iterable, prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    credits: https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
    """
    total = len(iterable)
    # Progress Bar Printing Function

    def printProgressBar(iteration):
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        filledLength = int(length * iteration // total)
        bar = fill * filledLength + '-' * (length - filledLength)
        print('\r{0} |{1}| {2}% {3}'.format(prefix, bar, percent, suffix), end=printEnd)

    # Initial Call
    printProgressBar(0)

    # Update Progress Bar
    for i, item in enumerate(iterable):
        yield item
        printProgressBar(i + 1)
    # Print New Line on Complete
    print()
