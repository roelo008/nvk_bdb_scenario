"""
Classifying continues 'Overstromingsdiepte Grote Kans' raster into discrete classes and preparing for use in MRT
Hans Roelofsen, 01-02-2021
"""

import rasterio as rio
import numpy as np
import sys
import affine

sys.path.append(r'c:\apps\proj_code\mrt\sample')
import PAD2BNL as pad
import mrt_helpers as mrt


src = r'w:\PROJECTS\Nvk_bdb\a_geodata\b_2050\a_brondata\14_overstromingskans_hoog_KEA\a_levering\Overstroming\PKS_Overstromingskenmerken_Overstroming_Overstromingsdiepte_grote kans_nvt_v2_t0_uit.tif'
rast = rio.open(src)

# Create bins, as used by KlimaatEffecten Atlas and digitize.
# The bins are:
#    0.2 > meter >= 0    --> bin 1
#    0.5 > meter >= 0.2  --> bin 2
#    2.0 > meter >= 0.5  --> bin 3
#    5.0 > meter >= 2.0  --> bin 4
# 1000.0 > meter >= 5.0  --> bin 5

bins = np.array([0, 0.2, 0.5, 2.0, 5, 1000])
classes = np.digitize(rast.read(1), bins)
classes = classes.astype(np.uint8)

# Create new rio object
profile = rast.profile
profile.update(dtype=rio.dtypes.ubyte, nodata=0, compress='LZW')
class_rast = mrt.create_rio(profile, classes)
del classes

# Resample to 2.5m
new_width = rast.width * 10
new_height = rast.height * 10
new_affine = affine.Affine(2.5, 0, rast.transform.c, 0, -2.5, rast.transform.f)
profile.update(width=new_width, height=new_height, transform=new_affine)
resamp_rast = mrt.create_rio(profile, class_rast.read(out_shape=(new_height, new_width),
                               resampling=rio.enums.Resampling.nearest)[0, :, :])

# Pad to BNL extent
pad.bnl_padding(source=resamp_rast,
                destination=r'c:/apps/temp_geodata/overstromingen/OverstromingsDiepteGroteKans_KEA.tif')
