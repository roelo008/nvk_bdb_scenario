"""
Classifying hitteeiland raster into discrete classes and preparing for use in MRT
Hans Roelofsen, 06-01-2021
"""

import rasterio as rio
import numpy as np
import sys
import affine

sys.path.append(r'c:\apps\proj_code\mrt\sample')
import PAD2BNL as pad
import mrt_helpers as mrt


src = r'W:\PROJECTS\NVk_bdb\a_geodata\b_2050\a_brondata\7_hitteeiland\a_levering\Hitte\PKS_Gevoelige functies en ruimtelijke kenmerken_Hitte_Stedelijk hitte eiland effect_nvt_v2_t0_uit.tif'
rast = rio.open(src)

# Create bins, as used by KlimaatEffecten Atlas and digitize.


bins = np.array([0, 0.2, 0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2, 1000])
classes = np.digitize(rast.read(1), bins)
classes = classes.astype(np.uint8)

# Create new rio object
profile = rast.profile
profile.update(dtype=rio.dtypes.ubyte, nodata=0, compress='LZW')
class_rast = mrt.create_rio(profile, classes)
del classes

# Resample to 2.5m
new_width = rast.width * 4
new_height = rast.height * 4
new_affine = affine.Affine(2.5, 0, rast.transform.c, 0, -2.5, rast.transform.f)
profile.update(width=new_width, height=new_height, transform=new_affine)
resamp_rast = mrt.create_rio(profile, class_rast.read(out_shape=(new_height, new_width),
                                                      resampling=rio.enums.Resampling.nearest)[0, :, :])

# Pad to BNL extent
pad.bnl_padding(source=resamp_rast,
                destination=r'c:/apps/temp_geodata/hitte/UrbanHeatIsland_KEA.tif')
