"""
Resampling 'Waterdiepte Korte Hevige Neerslag 1:100 jaar'
Hans Roelofsen, 02-02-2021
"""

"""
gdal_translate -tr 2.5 2.5 -ot Byte -r mode -co COMPRESS=LZW -mo CREATED=02-02-2021 -mo CREATED_BY=HANSROELOFSEN "PKS_Gevoelige functies en ruimtelijke kenmerken_Wateroverlast_Waterdiepte bij intense neerslag - 1-100 jaar_nvt_v2_t0_uit.tif" wdiepte_100j_KEA.tif
gdal_translate -outsize 112000 130000 -ot Byte -r nearest -a_ullr 0 625000 280000 300000 -co COMPRESS=LZW -mo CREATED=02-02-2021 -mo CREATED_BY=HANSROELOFSEN wdiepte_100j_KEA.tif wdiepte2_100j_KEA.tif 
"""