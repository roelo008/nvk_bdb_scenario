"""
Script to prepare zoutgehalte kaart for use in BDB scenario
Hans Roelofsen, WEnR, 16-02-2021
"""

import pandas as pd
import rasterio as rio
import numpy as np
import sys
import affine

sys.path.append(r'c:\apps\proj_code\mrt\sample')
import PAD2BNL as pad
import mrt_helpers as mrt

# Treshold for zoutgehalte
th = 2.18
out_name = 'ZOUTGEHALTE WORTELZONE_STOOM_th{}_1929_2011'.format(str(th).replace('.', '-'))

# Read source image
src = r'w:\PROJECTS\Nvk_bdb\a_geodata\b_2050\a_brondata\13_zoutgehalte_wortelzone\a_levering\ZOUTGEHALTE WORTELZONE_STOOM_1929_2011.ASC'
rast = rio.open(src)
arr = rast.read(1)

# Zeros array for output
arr_out = np.zeros(arr.shape, dtype=np.uint8)

# Where arrary is below or equal to treshold, set to 1
arr_out[np.where(arr <= th)] = 1

# Where array is above treshold, set to 2
arr_out[np.where(arr > th)] = 2

# Where array is equal to NoData, set to 0
arr_out[np.where(arr == rast.nodata)] = 0
del arr

## Note: np.digitize() had ook gekund hier.

# Create new rio object
profile = rast.profile
profile.update(dtype=rio.dtypes.ubyte, nodata=0, compress='LZW', driver='GTiff')
class_rast = mrt.create_rio(profile, arr_out)

# Resample to 2.5m
new_width = rast.width * 100
new_height = rast.height * 100
new_affine = affine.Affine(2.5, 0, rast.transform.c, 0, -2.5, rast.transform.f)
profile.update(width=new_width, height=new_height, transform=new_affine)
resamp_rast = mrt.create_rio(profile, class_rast.read(out_shape=(new_height, new_width),
                                                      resampling=rio.enums.Resampling.nearest)[0, :, :])

# Pad to BNL extent
pad.bnl_padding(source=resamp_rast,
                destination=os.path.join(r'c:/apps/temp_geodata/zout', '{}.tif'.format(out_name)))
tab = pd.DataFrame.from_dict({1: '<= 2.18', 2: '> 2.18', 0:'Nodata'}, columns=['Description'], orient='index')
tab['Value'] = tab.index
mrt.write_qml(tab, r'c:/apps/temp_geodata/zout', out_name)