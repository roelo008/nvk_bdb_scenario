"""
Resample and pad the BNL25 map to BNL specs
Hans Roelofsen, 30-12-2020
"""

import rasterio as rio
import numpy as np
import sys
import affine

sys.path.append(r'c:\apps\proj_code\mrt\sample')
import PAD2BNL as pad
import mrt_helpers as mrt

src = r'W:\PROJECTS\Nvk_bdb\a_geodata\b_2050\a_brondata\3_LBK\a_origineel\FG_Eenh25.tif'
rast = rio.open(src)

# Resample to 2.5m
new_width = rast.width * 10
new_height = rast.height * 10
new_affine = affine.Affine(2.5, 0, rast.transform.c, 0, -2.5, rast.transform.f)
profile = rast.profile
profile.update(width=new_width, height=new_height, transform=new_affine)
resamp_rast = mrt.create_rio(profile, rast.read(out_shape=(new_height, new_width),
                                                resampling=rio.enums.Resampling.nearest)[0, :, :])
# Pad to BNL extent
pad.bnl_padding(source=resamp_rast,
                destination=r'c:\apps\temp_geodata\LBK\middelgrote_overstromingskans_KEA.tif')
