"""
Short script for coupling LBK eenheid_codes and maatregelen.
LBK comes in two resolutions 500m en 25m. De eenheid van de LBK is de 'eenheid', geidentificeerd met een code,
naam en pixel value.

De Scenario opgaven zijn gekoppeld aan de eenheid_Codes van LBK500. Het raster waarop de maatregelen moeten worden
geprojecteerd is LBK25.

In dit script:
1. zijn alle eenheden uit LBK500 met een Opgave present in LBK25?
2. Aggregeer LBK25 eenheden naar Opgave

Hans Roelofsen, november 2020
"""

import os
import numpy as np
import pandas as pd
import random
import xml.etree.ElementTree as ET

def rand_web_color_hex():
    """"
    Generate random color.
    https://blog.stigok.com/2019/06/22/generate-random-color-codes-python.html
    """

    rgb = ""
    for _ in "RGB":
        i = random.randrange(0, 2**8)
        rgb += i.to_bytes(1, "big").hex()
    return '#{}'.format(rgb)


def write_qml(tab, lyr_name, out_dir, out_name, color=None, colorramp=False):
    """
    Write QGIS QML file
    :param tab:
    :param out_dir:
    :param out_name:
    :return:
    """

    # tree = ET.parse(r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\NVK_BDB\bdb_huidig_qgis_style_file.qml')
    tree = ET.parse(r'c:\apps\temp_geodata\LBK\FG_Eenh25.qlr')
    root = tree.getroot()
    maplayers = root.find('maplayers')
    maplayer = maplayers.find('maplayer')
    layername = maplayer.find('layername')
    pipe = maplayer.find('pipe')
    rasterrenderer = pipe.find('rasterrenderer')
    colorpalette = rasterrenderer.find('colorPalette')

    # sequential 5 category color ramp
    if colorramp:
        # Dit is niet zo netjes, maar vooruit
        vals = list(set(tab.Description))
        vals.sort()
        val2color = dict(zip(vals, ['#edf8fb','#bfd3e6','#9ebcda','#8c96c6','#8856a7','#810f7c'][:len(vals)]))

    # target = root[2][0][2]
    collor_pallete_target = root[1][0][12][0][2]  # Dit moet natuurlijk beter met root.find('
    assert collor_pallete_target.tag == 'colorPalette'

    for x in collor_pallete_target.findall('paletteEntry'):
        collor_pallete_target.remove(x)

    vals = tab.Value
    desc = tab.Description
    for v, d in zip(vals, desc):
        if colorramp:
            color = val2color[d]
        else:
            color = color
        ET.SubElement(collor_pallete_target, 'paletteEntry', attrib={'color': color if color else rand_web_color_hex(),
                     'value': str(v), 'label': str(d), 'alpha': "255"})

    # update layer name
    if hasattr(layername, 'text'):
        layername.text = lyr_name
    tree.write(os.path.join(out_dir, '{}.qlr'.format(out_name)))

# Read source data
lbk25_src = r'c:\Users\roelo008\Wageningen University & Research\Natuurverkenning breder doelbereik - Scenariokaart\LBK\FG_eenheid_r_Codes_25_vertaal.xlsx'
koppeling_sheet, opgave_sheet = 'koppelingLBK', 'OmschrijvingOpgaves'
lbk25 = pd.read_excel(lbk25_src, sheet_name=koppeling_sheet, dtype={'Scen_code': float,  'Scen_code2': float,
                                                                    'Scen_code3': float, 'Scen_code4': float,
                                                                    'Scen_code5': float},
                      na_values={'Scen_code': 99, 'Scen_code2': 99, 'Scen_code3': 99, 'Scen_code4': 99, 'Scen_code5': 99})
scens = [x for x in list(lbk25) if x.startswith('Scen')]
# lbk25.fillna(value={'Scen_code': 'geen'},  inplace=True)

opgaves = pd.read_excel(lbk25_src, sheet_name=opgave_sheet, dtype={'Meekoppeling': str, 'Code_opgave': float})
opgaves.Meekoppeling.fillna(value='geen', inplace=True)
maatregel2code = dict(zip(opgaves.Maatregel, opgaves.Code_opgave))
code2maatregel = dict(zip(opgaves.Code_opgave, opgaves.Maatregel))
code2deelopgave = dict(zip(opgaves.Code_opgave, [x.replace(' ', '_') for x in opgaves.Deelopgave]))

alle_opgaves = list(set(opgaves.Code_opgave))

# Primaire zoekgebieden
for code, deelopgave in code2deelopgave.items():
    qml_out_name = '{0}_{1}_Primo'.format(str(code).replace('.', '-'), deelopgave)
    lyr_name = 'PrimairZG: {0}: {1}'.format(code, deelopgave)

    temp_tab = lbk25.loc[lbk25.Scen_code  == code, :]
    maatregel = code2maatregel[code]

    if not temp_tab.empty:
        temp_tab.loc[:, 'Description'] = temp_tab.apply(lambda row: '{0}: {2} MET {1}'.format(code, maatregel,
                                                                                              row.Naam_Eenh), axis=1)
        write_qml(tab=temp_tab.loc[:, ['ID', 'Description']].rename(columns={'ID': 'Value'}), color="#f1480b",
                  lyr_name=lyr_name, out_dir=r'c:\apps\temp_geodata\LBK', out_name=qml_out_name)
    else:
        print('Empty df for {}'.format(code))

# Secundaire zoekgebieden
for code, deelopgave in code2deelopgave.items():
    qml_out_name = '{0}_{1}_Secundo'.format(str(code).replace('.', '-'), deelopgave)
    lyr_name = 'SecundairZG: {0}: {1}'.format(code, deelopgave)

    maatregel = code2maatregel[code]
    temp_tab = lbk25.loc[(lbk25.Scen_code2 == code) |
                         (lbk25.Scen_code3 == code) |
                         (lbk25.Scen_code4 == code) |
                         (lbk25.Scen_code5 == code), :]
    if not temp_tab.empty:
        temp_tab.loc[:, 'Description'] = temp_tab.apply(lambda row: '{0}: {2} MET {1}'.format(code, maatregel,
                                                                                              row.Naam_Eenh), axis=1)
        write_qml(tab=temp_tab.loc[:, ['ID', 'Description']].rename(columns={'ID': 'Value'}), color="#afafaf",
                  lyr_name=lyr_name, out_dir=r'c:\apps\temp_geodata\LBK', out_name=qml_out_name)
    else:
        print('Empty df for {}'.format(code))

# Stapeling van opgaves geeft hoeveel mogelijkheden per gebied?
lbk25.loc[:, 'n_opgaves'] = lbk25.loc[:, scens].count(axis=1)
write_qml(tab=lbk25.loc[:, ['ID', 'n_opgaves']].rename(columns={'ID': 'Value', 'n_opgaves': 'Description'}),
          lyr_name='AantalOpgaves', out_dir=r'c:\apps\temp_geodata\LBK', out_name='AantalOpgaves', colorramp=True)